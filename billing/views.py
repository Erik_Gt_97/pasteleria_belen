from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView
from .models import bill, billDetail
from .forms import billForm, billDetailForm, DetailBillFormSet

# Creando lista de facturas
class billList(LoginRequiredMixin, ListView):
    login_url = 'login'
    model = bill
    template_name = "billing/bill_list.html"
    context_object_name = "facturas"

class billNew(PermissionRequiredMixin, CreateView):
    permission_required = 'billing.add_bill'
    model = bill
    login_url = 'core:home'
    template_name = 'billing/bill_form.html'
    form_class = billForm
    success_url = reverse_lazy('billing:bill_list')

    #cargar formulario vacío cuando se ejecute la vista
    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class() #reconocemos factura
        form = self.get_form(form_class) #cargamos los datos de la factura en form
        detalle_bill_formset = DetailBillFormSet() #instanciamos detalles
        return self.render_to_response(self.get_context_data(
            form = form,
            detail_bill = detalle_bill_formset
        ))
    
    #sobreescribiendo método post
    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        detail_bill = DetailBillFormSet(request.POST) #obtener datos con lo que se pasó en el post

        #validando los datos de la factura y de los detalles
        if form.is_valid() and detail_bill.is_valid():
            return self.form_valid(form, detail_bill)
        else:
            return self.form_invalid(form, detail_bill)
    
    #personalizamos los métodos de validación
    def form_valid(self, form, detail_bill):
        self.object = form.save() #guardamos la factura
        detail_bill.instance = self.object #cargamos una instancia del conjunto de detalles vinculados a la factura
        detail_bill.save() #guardamos esa instancia
        return HttpResponseRedirect(self.success_url)
    
    def form_invalid(self, form, detail_bill):
        return self.render_to_response(
            #lo regresamos a como estaba con los datos del formulario antes de que haga el envío
            self.get_context_data(
                form = form,
                detail_bill = detail_bill
            )
        )



class billEdit(PermissionRequiredMixin, UpdateView):
    permission_required = 'billing.change_bill'
    model = bill
    login_url = 'core:home'
    template_name = 'billing/bill_form.html'
    form_class = billForm
    success_url = reverse_lazy('billing:bill_list')

    def get_success_url(self):
        from django.urls import reverse
        #redireccionamos el success_url
        return reverse('billing:bill_edit', kwargs={'pk':self.get_object().id})
    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        detail = billDetail.objects.filter(bill=self.object).order_by('pk')
        detail_data = []
        for det in detail:
            d = {
                'design':det.design,
                'cantidad':det.cantidad,
                'price':det.price,
                'total':det.total
            }
            detail_data.append(d)
        
        detail_bill = DetailBillFormSet(initial=detail_data)
        detail_bill.extra += len(detail_data)
        return self.render_to_response(self.get_context_data(
            form = form,
            detail_bill = detail_bill
        ))
    
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        detail_bill = DetailBillFormSet(request.POST)
        if form.is_valid() and detail_bill.is_valid():
            return self.form_valid(form, detail_bill)
        else:
            return self.form_invalid(form, detail_bill)
    
    def form_valid(self, form, detail_bill):
        self.object = form.save()
        detail_bill.instance = self.object
        billDetail.objects.filter(bill=self.object).delete()
        detail_bill.save()
        return HttpResponseRedirect(self.get_success_url())
    
    def form_invalid(self, form, detail_bill):
        return self.render_to_response(
            #lo regresamos a como estaba con los datos del formulario antes de que haga el envío
            self.get_context_data(
                form = form,
                detail_bill = detail_bill
            )
        )