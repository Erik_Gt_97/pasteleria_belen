from django.urls import path
from billing.views import billList, billNew, billEdit

urlpatterns = [
    path('facturas/', billList.as_view(), name = "bill_list"),
    path('factura/new/', billNew.as_view(), name = "bill_new"),
    path('factura/edit/<int:pk>', billEdit.as_view(), name = "bill_edit"),
]