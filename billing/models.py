from django.db import models
from core.models import ClaseModelo
from catalog.models import design
from decimal import Decimal

# Creando modelos de maestro detalle

class bill(ClaseModelo): #factura
    date_bill = models.DateField()
    observation = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.date_bill)

    class Meta:
        verbose_name = "Factura"
        verbose_name_plural = "Facturas"

class billDetail(ClaseModelo): #detalle factura
    bill = models.ForeignKey(bill, on_delete=models.CASCADE)
    design = models.ForeignKey(design, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=6, decimal_places=2, blank=False, null=False, default=0)
    total = models.DecimalField(max_digits=6, decimal_places=2, default=0)

    def __str__(self):
        return '{} - {}'.format(self.bill, self.design)
    
    def save(self):
        self.total = Decimal(self.cantidad) * Decimal(self.price)
        super(billDetail, self).save()

    class Meta:
        verbose_name = "Detalle de Factura"
        verbose_name_plural = "Detalles de Facturas"