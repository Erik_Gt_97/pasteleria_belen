from django import forms
from django.forms.models import inlineformset_factory
from billing.models import bill, billDetail
from catalog.models import design

class billForm(forms.ModelForm):
    date_bill = forms.DateInput()

    class Meta:
        model = bill
        fields = ['date_bill', 'observation']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class' : 'form-control'
            })

class billDetailForm(forms.ModelForm):
    design = forms.ModelChoiceField(
        queryset=design.objects.filter(activo=True).
        order_by("description"),
        empty_label="Seleccione Producto"
    )

    class Meta:
        model = billDetail
        fields = ['design', 'cantidad', 'price', 'total']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
        self.fields['total'].widget.attrs['readonly'] = True
    
    #si sucede algo se queda en el error, sino devuelve la cantidad
    def clean_cantidad(self):
        cantidad = self.cleaned_data['cantidad']
        if not cantidad:
            raise forms.ValidationError("Cantidad Requerida")
        elif cantidad <= 0:
            raise forms.ValidationError("Cantidad Incorrecta")
        return cantidad
    
    def clean_price(self):
        price = self.cleaned_data['price']
        if not price:
            raise forms.ValidationError("Precio Requerido")
        elif price <= 0:
            raise forms.ValidationError("Precio Incorrecto")
        return price

#Vinculamos los dos modelos de factura y detalles a un formulario
DetailBillFormSet = inlineformset_factory(bill, billDetail, form=billDetailForm, extra=4)